#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

uniform sampler2D texture;
uniform mat4 texMatrix;
uniform vec2 texOffset;

uniform float kick;

varying vec4 vertColor;
varying vec4 vertTexCoord;

vec2 applyKick(vec2 coord)
{
	return (coord - 0.5) * (1.0 - kick * 0.1) + 0.5;
}

void main(void)
{
	gl_FragColor = texture2D(texture, vertTexCoord.xy);
}