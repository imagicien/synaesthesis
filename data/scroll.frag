#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

#define PI 3.14159265359

uniform sampler2D texture;
uniform mat4 texMatrix;
uniform vec2 texOffset;

uniform sampler2D spectrumData;
uniform float speed;
uniform float stretchFactor;
uniform mat2  rotation;
uniform vec2  rotAnchor;
uniform vec3 color;
uniform float cymbal;

varying vec4 vertColor;
varying vec4 vertTexCoord;

vec3 rgb2hsv(vec3 c);
vec3 hsv2rgb(vec3 c);

vec3 colorProg1(float magnitude)
{
    float hueColorWeight = 1.0 - 2.0 * abs(magnitude - 0.5);
    vec3 weightedHueColor = color * hueColorWeight;
    float whiteWeight = max(2 * magnitude - 1.0, 0.0);
    vec3 weightedWhite = vec3(1.0) * whiteWeight;
    return weightedHueColor + weightedWhite;
}

#define ONE_THIRD 0.333333
#define TWO_THIRDS 0.666666

vec3 colorProg2(float x)
{
    vec3 color1 = color;
    vec3 c2hsv = rgb2hsv(color);
    vec3 color2 = hsv2rgb(c2hsv + vec3(0.5, 0.0, 0.0));

    //float weight1 = max(0.0, 1.0 - 3.0 * abs(x - ONE_THIRD));
    //float weight2 = max(0.0, 1.0 - 3.0 * abs(x - TWO_THIRDS));
    vec2 weights_1_2 = max(vec2(0.0), 1.0 - 3.0 * abs(vec2(
        x - ONE_THIRD,
        x - TWO_THIRDS
        )));
    float weight3 = max(0.0, 3.0 * (x - TWO_THIRDS));

    return weights_1_2.x * color1
         + weights_1_2.y * color2
         + weight3       * vec3(1.0);
}

vec3 colorProg3(float x)
{
    vec3 color1 = color;
    vec3 c2hsv = rgb2hsv(color);
    vec3 color2 = hsv2rgb(c2hsv + vec3(-0.2, 0.0, 0.0));
    vec3 color3 = hsv2rgb(c2hsv + vec3( 0.2, 0.0, 0.0));

    //float weight1 = max(0.0, 1.0 - 3.0 * abs(x - ONE_THIRD));
    //float weight2 = max(0.0, 1.0 - 3.0 * abs(x - TWO_THIRDS));
    vec3 weights = max(vec3(0.0), 1.0 - 4.0 * abs(vec3(
        x - 0.25,
        x - 0.5,
        x - 0.75
        )));
    float weight4 = max(0.0, 4.0 * (x - 0.75));

    return weights.x * color1
         + weights.y * color2
         + weights.z * color3
         + weight4   * vec3(1.0);
}

vec3 makeColor()
{
    float magnitude = texture2D(spectrumData, vec2(0.0, vertTexCoord.y)).r;
    
    float posy = 2.0 * abs(vertTexCoord.y - 0.5);
    float cymbalAmount = cymbal * (posy - 1.0) * (posy - 1.0);
    magnitude += 0.65 * cymbalAmount;

    vec3 blendedColor = colorProg3(magnitude);

    return blendedColor;
}

void main(void)
{
    float stretchOffset = 1.0 - stretchFactor * abs(0.5 - vertTexCoord.x);
    vec2 pos = vec2(vertTexCoord.x + sign(0.5 - vertTexCoord.x) * speed * texOffset.x, (vertTexCoord.y - 0.5) * stretchOffset + 0.5);

    // Rotation
    mat2 actualRot;
    if (pos.x < 0.5)
    {
        actualRot = rotation;
    }
    else
    {
        actualRot = transpose(rotation);
    }
    vec2 rotated = actualRot * (pos - rotAnchor);
    vec2 vecScreen = rotated + rotAnchor;

    // 0.5 + 0.5 * cos(PI * x) => -1..1
    float seedAmount = 0.0;
    float centeredPos = vertTexCoord.x - 0.5;
    //if (abs(centeredPos) < 0.01)
    //    seedAmount = 1.0;
    const float width = 0.02;
    const float cosScale = 1.0 / width;
    if(abs(centeredPos) < width)
        seedAmount = 0.5 + 0.5 * cos(cosScale * PI * centeredPos);
    
    vec3 seedColor = makeColor();
    vec3 bgColor = texture2D(texture, vecScreen).rgb;
    vec3 blendedColor = mix(bgColor, seedColor, seedAmount);

    gl_FragColor = vec4(blendedColor, 1.0);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}