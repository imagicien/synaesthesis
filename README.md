# Synæsthesis #

*par Carl Lemaire*

La représentation visuelle d'un son la plus perceptuellement fidèle pouvant être représentée sur un écran est le [*sonagramme*](https://fr.wikipedia.org/wiki/Sonagramme).  
Le présent projet explore cette prémisse, et tente d'associer judicieusement les trois dimensions d'un écran (deux axes spatiaux et temps) avec les trois dimensions d'un son (fréquence, intensité et temps).