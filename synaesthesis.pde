/*
TODO
  - Modulariser
  - Balancer le contraste: plusieurs zones de contraste, interp
  - Multi-hue
  - Bouger le seed
  
  - Performance
  - Controle: play/pause/restart
*/

import ddf.minim.analysis.*;
import ddf.minim.*;
import java.util.Map;
import java.awt.Color;

import org.gamecontrolplus.gui.*;
import org.gamecontrolplus.*;
import net.java.games.input.*;

Minim minim;  
AudioPlayer jingle;
FFT fftLog;

ControlIO control;
Configuration config;
ControlDevice gpad;

int R = 0;
int G = 1;
int B = 2;

String[] songFiles;

ArrayList<color[]> gradientBank;
ArrayList<String> gradientNames;
int currentGradient = 0;
int lastGradientSampleIndex = 999;

float maxVal;
int curColorCurve = 1;
float[] colorCurves = {10.0, 32.0, 200.0, 1000.0};
String colorCurvesKeys = "vbnm";
HashMap<Character, Integer> colorCurveKeyMap = new HashMap<Character, Integer>();

String availableKeys = "1234567890qwertyuiop";
HashMap<Character, Integer> keyMap = new HashMap<Character, Integer>();
Boolean doDisplayGradientNames = false;
Boolean doClearScreen = false;
Boolean isDistortedMode = true;
Boolean isShiftDown = false;

PGraphics canvas;
PShader shader;
float scrollSpeed = 1.0;
float lastDistortingSpeed;
float scrollStretchFactor = 0.008;
float lastStrechFactor;

PImage spectrumData;
color currentColor;
float cymbalValue = 0;
float kickValue = 0;

float frameRateAvgTotal = 0;
float frameRateAvgSamples = 0;

float lastTime;

void setup() 
{
  size(900, 900, P2D);
  canvas = createGraphics(width, height, P2D);
  
  frameRate(28);
  colorMode(RGB, 1.0);
  surface.setTitle("Synaesthesis");
  
  shader = loadShader("scroll.frag");
  shader.set("speed", scrollSpeed);
  shader.set("stretchFactor", scrollStretchFactor);
  setRotation(0.0);
  
  spectrumData = createImage(1, height, RGB);
  shader.set("spectrumData", spectrumData);
  
  currentColor = color(1.0, 0.0, 0.0);
  shader.set("color", 1.0, 0.0, 0.0);
  
  // Key Mapping
  for(int i = 0; i < availableKeys.length(); i++)
    keyMap.put(availableKeys.charAt(i), i);
  for(int i = 0; i < colorCurvesKeys.length(); i++)
    colorCurveKeyMap.put(colorCurvesKeys.charAt(i), i);
  
  // Gradient bank initialisation
  //println("Sampling color gradients...");
  //generateGradientBank(lastGradientSampleIndex + 1);
  //println("Done.");
  
  //int foundIndex = gradientNames.indexOf("Granny Smith");
  //currentGradient = foundIndex != -1 ? foundIndex : 0;
  
  songFiles = new String[100];
  songFiles[0] = "mastersAp.mp3";
  songFiles[1] = "painIsee.mp3";
  songFiles[2] = "gone.mp3";
  songFiles[3] = "06-Divine Ecstasy.mp3";
  songFiles[4] = "marcus_kellis_theme.mp3";
  songFiles[5] = "vent_inerte1.wav";
  songFiles[6] = "versOuCourir.wav";
  songFiles[7] = "heritage.wav";
  songFiles[8] = "porcelainHeart.wav";
  songFiles[9] = "coil.wav";
  songFiles[10] = "01 Johnny Delusional.mp3";
  songFiles[11] = "07 So Desu Ne.mp3";
  songFiles[12] = "08 The Man Without A Tan.mp3";
  songFiles[13] = "Bee Gees - Staying Alive.mp3";
  songFiles[14] = "Nicotine.mp3";
  songFiles[15] = "Up To You.mp3";
  songFiles[16] = "insignificance.mp3";
  songFiles[17] = "coldblood track1.wav";
  songFiles[18] = "publictransportation.wav";
  songFiles[19] = "16 A Violent Death.mp3";
  songFiles[20] = "07-Peak Experience.mp3";
  songFiles[21] = "coldblood - 0 percent.wav";
  int selectedSong = 3;

  //String fileName = args.length > 0 ? args[0] : songFiles[selectedSong];
  String fileName = "music/" + songFiles[selectedSong];

  minim = new Minim(this);
  jingle = minim.loadFile(fileName, 1024);
  //maxVal = pow(2, jingle.getFormat().getSampleSizeInBits())-1;
  maxVal = 50;
  println("maxVal: ", maxVal);
  
  // loop the file
  jingle.loop();
  
  // create an FFT object for calculating logarithmically spaced averages
  fftLog = new FFT( jingle.bufferSize(), jingle.sampleRate() );
  
  fftLog.window(FFT.BLACKMAN);
  
  // Gamepad =======================
  // Initialise the ControlIO
  control = ControlIO.getInstance(this);
  // Find a device that matches the configuration file
  gpad = control.getMatchedDevice("synaesthesisController");
  if (gpad == null) {
    println("No suitable device configured");
    System.exit(-1); // End the program NOW!
  }
  
  //background(getGradientColor(0));
  lastTime = millis();
}

void draw()
{ 
  //frameRateAvgTotal += frameRate;
  //float frameRateAvg = frameRateAvgTotal / ++frameRateAvgSamples;
  //if(frameCount % 200 == 0)
  //  println(frameRateAvg);
  
  float curTime = millis();
  float deltaTime = (curTime - lastTime) / 1000;
  lastTime = curTime;
  
  // Draw last frame distorted by shader
  image(canvas, 0, 0, width, height);
  
  fftLog.forward( jingle.mix );
  
  maxVal *= 0.99;
  
  spectrumData.set(0, 0, color(0, 0, 0));
  spectrumData.set(0, height-1, color(0, 0, 0));
  for(int y = 1; y < height-1; y++) // Leave 1st and last pixel black for better shrink effect
  {
   float valeurInterpolee = fractionalSpectrum(fftLog, y);
   if (valeurInterpolee > maxVal) maxVal = valeurInterpolee;
   float fracValue = valeurInterpolee / maxVal;
   
   float b = colorCurves[curColorCurve];
   float logValue = min(1.0, logThatShit(b, (b+4.0)*fracValue + 1.0));
   
   spectrumData.set(0, height-1 - y, color(logValue, 0, 0));
  }
  shader.set("spectrumData", spectrumData);
  
  canvas.beginDraw();
  canvas.shader(shader);
  canvas.image(get(), 0, 0, width, height);
  canvas.stroke(0, 0, 0);
  canvas.resetShader();
  canvas.line(0, 0, width, 0);
  canvas.line(0, height-1, width, height-1);
  canvas.endDraw();
  
  if (doDisplayGradientNames)
  {
    // Display gradient names
    noStroke();
    fill(0);
    rect(0, 0, 200, 40 + gradientNames.size() * 20);
    for(int i = 0; i < gradientNames.size(); i++)
    {
      fill(i == currentGradient ? color(1, 1, 0) : color(1, 1, 1));
      text(gradientNames.get(i),    40, 30 + i * 20);
      if (i >= availableKeys.length()) continue;
      text(availableKeys.charAt(i), 10, 30 + i * 20);
    }
    noFill();
  }
  
  // Game pad =======
  if (gpad.getButton("B4").pressed())
  {
    cymbalValue = 1.0;
  }
  else if(cymbalValue > 0.0)
  {
    cymbalValue = max(0.0, cymbalValue - 2.0 * deltaTime);
  }
  shader.set("cymbal", cymbalValue);
  if (gpad.getButton("B3").pressed())
  {
    kickValue = 1.0;
  }
  else if(kickValue > 0.0)
  {
    kickValue = max(0.0, kickValue - 16.0 * deltaTime);
  }
  
  
  //int hatPos = gpad.getHat("Croix").getPos();
  // Up-left: 1, up: 2, right: 4, down: 6, left: 8
  
  float stickGX = gpad.getSlider("StickGX").getValue();
  float stickGY = -1.0 * gpad.getSlider("StickGY").getValue();
  float stickDX = gpad.getSlider("StickDX").getValue();
  float stickDY = -1.0 * gpad.getSlider("StickDY").getValue();
  
  float speed = 1.0;
  if (gpad.getButton("LB").pressed()) // LB: Stretch + Rotate
  {
    if (stickGX != 0.0)
      setScrollStretchFactor(signedSquare(stickGX) * 0.3);
    if (stickGY != 0.0)
      setRotation(signedSquare(stickGY) * 0.02);
  }
  if (gpad.getButton("LT").pressed()) // RT: Speed + ?
  {
    if (stickGX != 0.0)
    {
      speed = 3.0 * (stickGX + 1.0)*(stickGX + 1.0);
    }
  }
  if (gpad.getButton("RB").pressed()) // RB: Color
  {
    float newHue = constrain(atan2(stickDY, stickDX) / (2.0 * PI) + 0.5, 0.0, 1.0);
    color newColor = Color.HSBtoRGB(newHue, 1.0, 1.0);
    float weight = sqrt(stickDX * stickDX + stickDY * stickDY);
    if (weight > 0.01)
    {
      weight = 0.5 * pow(weight, 4.0);
      //println("w:" + weight + "  h:" + colorToString(currentColor) + "  nh:" + colorToString(newColor));
      currentColor = lerpColor(currentColor, newColor, weight);
    }
  }
  
  setScrollSpeed(speed * (1.0 + kickValue * 1.5));
  
  // Change saturation (cymbal)
  //color drawColor = lerpColor(currentColor, color(1.0, 1.0, 1.0), cymbalValue * 0.65);
  shader.set("color",
    red(currentColor),
    green(currentColor),
    blue(currentColor)
    );
}

String colorToString(color c)
{
  return Float.toString(hue(currentColor))
  + "-" + Float.toString(saturation(currentColor))
  + "-" + Float.toString(brightness(currentColor));
}

float signedSquare(float value)
{
  return sign(value) * value * value;
}

float sign(float value)
{
  if (value > 0.0) return 1.0;
  else if (value < 0.0) return -1.0;
  else return 0.0;
}

void setRotation(float angle)
{
  PMatrix2D mat = new PMatrix2D();
  mat.rotate(angle);
  shader.set("rotation", mat);
  if (angle > 0.0)
    shader.set("rotAnchor", 0.5, 0.0);
  else
    shader.set("rotAnchor", 0.5, 1.0);
}
/*
void generateGradientBank(int nbSamples)
{
  gradientBank = new  ArrayList<color[]>();
  gradientNames = new ArrayList<String>();
  
  JSONArray jsonBank = loadJSONArray("gradientBank.txt");
  for(int g = 0; g < jsonBank.size(); g++)
  {
    // An entry is an array: [name, listOfColours (, maxVal)]
    JSONArray gradientEntry = jsonBank.getJSONArray(g);
    
    gradientNames.add(gradientEntry.getString(0));
    
    float maxVal = 1;
    if (gradientEntry.size() == 3)
      maxVal = gradientEntry.getFloat(2);
    
    JSONArray jsonSteps = gradientEntry.getJSONArray(1);
    ArrayList<float[]> gradientSteps = new ArrayList<float[]>();
    for(int c = 0; c < jsonSteps.size(); c++)
    {
      JSONArray colour = jsonSteps.getJSONArray(c); 
      gradientSteps.add(new float[]{ 
        colour.getFloat(R) / maxVal,
        colour.getFloat(G) / maxVal,
        colour.getFloat(B) / maxVal
      });
    }
    gradientBank.add(generateGradient(gradientSteps, nbSamples));
  }
}

color[] generateGradient(ArrayList<float[]> steps, int nbSamples)
{  
  //Deactivated for Live Palette
  //colorMode(RGB, 1.0);
  
  int NB_STEPS = steps.size();
  steps.add(new float[]{0, 0, 0}); // Will be retrieved but will be multiplied by 0
   
  color[] gradientSamples = new color[nbSamples];
  
  for(int i = 0; i < nbSamples; i++)
  {
    float fracIndex = (float)i / (nbSamples-1) * (NB_STEPS-1);
    int index = (int)fracIndex;
    float u = fracIndex - index;
     
    float[] prev = steps.get(index);
    float[] next = steps.get(index + 1); 
    
    float r = (1.0 - u) * prev[R] + u * next[R]; 
    float g = (1.0 - u) * prev[G] + u * next[G];
    float b = (1.0 - u) * prev[B] + u * next[B];
    gradientSamples[i] = color(r, g, b);
  }
  
  return gradientSamples; 
} 
*/
float fractionalSpectrum(FFT fft, int yPos)
{
  float fracPos = (float)yPos / height;
  float fracSpec = logarithmize(fracPos);
  float fracBin = fracSpec * (fft.specSize()-1);
  int iBin1 = (int)fracBin;
  int iBin2 = iBin1 + 1;
  float u = fracBin - iBin1;
  float val = (1.0-u) * fft.getBand(iBin1) + u * fft.getBand(iBin2);
  return val;
  //return val / (3.0 * (pow(100.0, -fracSpec) - 0.01)); // Corrige la puissance des basses
}

float logarithmize(float x)
{
    return (1.0/(16.0*16.0))*(pow(16, 2.0*x)-1.0);
}

//color getGradientColor(float value)
//{ 
//  return gradientBank.get(currentGradient)[(int)(value * lastGradientSampleIndex)];
//}

float logThatShit(float base, float value)
{
  return log(value) / log(base);
}
/*
void keyPressed()
{
  if (key == CODED && keyCode == SHIFT)
    isShiftDown = true;
}

void keyReleased()
{
  if (key == CODED)
  {
    if (keyCode == LEFT || keyCode == UP)
    {
      currentGradient = currentGradient > 0 ? currentGradient - 1 : gradientBank.size() - 1;
    }
    else if(keyCode == RIGHT || keyCode == DOWN)
    {
      currentGradient = currentGradient < gradientBank.size() - 1 ? currentGradient + 1 : 0;
    }
    else if(keyCode == CONTROL)
    {
      doClearScreen = true;
    }
    else if(keyCode == SHIFT)
    {
      isShiftDown = false;
    }
  }
  else
  {
    if (keyMap.containsKey(key))
    {
      int index = keyMap.get(key);
      if (index < gradientBank.size())
          currentGradient = index;
    }
    else if (key == ' ')
    {
      doDisplayGradientNames = !doDisplayGradientNames; 
    }
    else if (key == 'z')
    {
      isDistortedMode = !isDistortedMode;
      if (isDistortedMode)
      {
        scrollSpeed = lastDistortingSpeed;
        scrollStretchFactor = lastStrechFactor;
      }
      else
      {
        lastDistortingSpeed = scrollSpeed;
        scrollSpeed = max(1, round(scrollSpeed));
        lastStrechFactor = scrollStretchFactor;
        scrollStretchFactor = 0.0;
      }
      shader.set("speed", scrollSpeed);
      shader.set("stretchFactor", scrollStretchFactor);
      println("Scroll speed: ", scrollSpeed);
      println("Scroll stretch factor: ", scrollStretchFactor);
    }
    else if (colorCurveKeyMap.containsKey(key))
    {
      curColorCurve = colorCurveKeyMap.get(key);
      println("Color curve: ", colorCurves[curColorCurve]);
    }
  }
}
*/
void setScrollStretchFactor(float value)
{
    scrollStretchFactor = constrain(value, -0.3, 0.3);
    shader.set("stretchFactor", scrollStretchFactor);
}

void setScrollSpeed(float value)
{
    scrollSpeed = constrain(value * 3.0, 0.5, 20.0);
    shader.set("speed", scrollSpeed);
}
/*
void mouseWheel(MouseEvent event)
{
  float e = event.getCount();
  isDistortedMode = true;
  if (isShiftDown)
  {
    setScrollStretchFactor(scrollStretchFactor - e * 0.001);
  }
  else
  {
    setScrollSpeed(scrollSpeed + e * 0.1);
  }
}
*/